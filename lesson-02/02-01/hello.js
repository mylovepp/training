//*********** Method 1 *****************
var helloWorld = function () {
    console.log('Hello World');
}

module.exports = helloWorld;

//*********** Method 2 *****************
// module.exports = function () {
//     console.log('Hello World');
// }


