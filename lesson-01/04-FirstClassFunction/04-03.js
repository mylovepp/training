
function start() {
    return function helloWorld() {
        console.log('Hello World');
    }
}

var hello = start();
hello();