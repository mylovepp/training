function printTotal(message) {
    return function calculateResult(calFn){
        console.log('%s => %s', message, calFn());
    }
}

function calculate() {
    var number1 = 5;
    var number2 = 10;

    function add() {
        return number1 + number2;
    }

    return add;
}

var addFn = calculate();
var print = printTotal('total');
print(addFn);
