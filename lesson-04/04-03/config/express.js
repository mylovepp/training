var config = require('./config');
var express = require('express');
var morgan = require('morgan');
var compression = require('compression');
var bodyPraser = require('body-parser');
var validator = require('express-validator');
var session = require('express-session');
var flash = require('connect-flash');
var passport = require('passport');
var MongoStore = require('connect-mongo')(session);

module.exports = function(){
    var app = express();

    
    if(process.env.NODE_ENV==='development'){
        app.use(morgan('dev'));
    }else{
        app.use(compression());
    }
   
    //=====Save Session in server's' memory
    // app.use(session({
    //     secret: config.sessionSecret,
    //     resave: false,
    //     saveUninitialized: true
    // }));
    //=====Save Session in MongoDB
    app.use(session({
        secret: config.sessionSecret,
        resave: false, //don't save session if unmodified
        collection: 'sessions',
        stringify: true,
        store: new MongoStore({
            url: config.mongoUri,
            ttl: 10*60, //Session timeout 10 miniutes
            autoRemove: 'interval',
            autoRemoveInterval: 10 // In minutes. Default
        })
    }));


    
    app.use(flash());
    app.use(passport.initialize());
    app.use(passport.session()); //need to use express-session

    app.use(bodyPraser.urlencoded({
        extended: true
    }));

    app.use(bodyPraser.json());
    app.use(validator());

    require('../app/routes/index.routes')(app);
    require('../app/routes/user.routes')(app);

    return app;

    
};