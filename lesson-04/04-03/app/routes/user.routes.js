var user = require('../controllers/user.controller');
var passport = require('passport');

module.exports = function(app){

    app.route('/signup')
        .post(user.signup);

    app.route('/login')
        .post(passport.authenticate('local', {
            successRedirect: '/login/true',
            failureRedirect: '/login/false',
            failureFlash: false
        }));
    app.get('/login/:status', user.loginResult);
    
    app.post('/logout', user.logout);

    app.route('/user')
        .post(user.create)
        .get(user.list);

    app.route('/user/:username')
        .get(user.userDetail)
        .put(user.update)
        .delete(user.delete);

    app.param('username', user.userByUsername); //hook once parameter have username

};