var User = require('mongoose').model('User');

exports.create = function(req, res, next){
    var user = new User(req.body);

    user.save(function(err){
        if(err){
            return next(err);
        }
        else{
            res.json(user);
        };
    });
};

exports.list = function(req, res, next){

    User.find({},'username firstName lastName email', {
        skip: 0,
        limit: 10
    }, function(err, users){
        if(err){
            return next(err);
        }else{
            res.json(users);
        };
    });
};

exports.userDetail = function(req, res, next){
    res.json(req.user);
};

exports.update = function(req, res, next){
    User.findOneAndUpdate({
        username: req.user.username
    }, req.body, {
        new: true
    },
    function(err, user){
        if(err){
            return next(err);
        }else{
            console.log(user);
            res.json(user);
        };
    });
};

exports.delete = function(req, res, next){
    req.user.remove( function(err, user){
        if(err){
            return next(err);
        }else{
            console.log(user);
            res.json(user);
        };
    });
};

exports.userByUsername = function(req, res, next, username){
    User.findOne({
        username: username
    }, '', 
    function(err, user){
        if(err){
            return next(err);
        }else{
            req.user = user;
            next();
        };
    });
};





// // exports.login = function(req, res){
// //     req.checkBody('username', 'username is empty').notEmpty();
// //     req.checkBody('password', 'password is empty').notEmpty();

// //     var errors = req.validationErrors();
// //     if(errors)
// //     {
// //         res.send(JSON.stringify(errors));
// //         return;
// //     }
// //     else{
// //         //Session Management
// //         // if(req.body.remember === true){
// //         //     req.session.username = req.body.username;
// //         //     req.session.cookie.maxAge = 60000; //millisec
// //         // }
// //     }

// //     var profile = {username: req.body.username, status: 'ok'};
// //     res.send(profile);
// // };

exports.logout = function(req, res){
    req.session = null;
};

exports.signup = function(req, res, next){
    //if(!req.user){
        var user = new User(req.body);
        user.provider = 'local';

        user.save(function(err){
            if(err){
                var message = getErrorMessage(err);

                //set message to flash (use for web page)
                //req.flash('error', message);

                //return message from flash
                // res.json({status: false, description: message});
                res.json(err);
            }

            //===== req.login provide from passport
            // req.login(user, function(err){
            //     if(err){
            //         return next(err);
            //     }else{ 
            //         res.json({status: true, description: 'login successful'});
            //     }
            // });
        })
    //}else{
    //    res.json({status: false, description: 'This account already exists'})
        //return res.redirect('/');
    //}
};

exports.loginResult = function(req, res){
    console.log(req.session.user);
    if(req.params.status && req.params.status === 'true'){
        req.session.currentUser = res.user;
        res.json({status:true, description:'Login success'});
    }else{
        req.session.currentUser = null;
        res.json({status:false, description:'Login fauilre'});
    };
};

exports.logout = function(req, res){
    req.logout();
    res.json({status:true, description:'Logged out'});
};

var getErrorMessage = function(err){
    var message = '';
    if(err.code){
        switch(err.code){
            case 11000:
            case 11001:
                message = 'Username already exists';
                break;
            default:
                message = 'Something went wrong';
        }
    }
    else{
        for(var errName in err.errors){
            if(err.errors[errName].message){
                message = err.errors[errName].message;
            }
        }
    };
    return message;

};

