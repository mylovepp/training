/*
var connect = require('connect');
var app = connect();

var helloWorld = function (req, res, next) {
    res.setHeader('Content-Type', 'text/plain');
    res.write('Hello World');
    res.end();
}

app.use('/', helloWorld);

app.listen(8080);

console.log('Server running as http://localhost:8080');
*/

var express = require('express');
var app = express();

var helloWorld = function (req, res, next) {
    // res.setHeader('Content-Type', 'text/plain');
    // res.write('Hello World');
    // res.end();

    res.send('Hello World');
}

app.use('/', helloWorld);

app.listen(8080, function () {
    console.log('Server running as http://localhost:8080');
});

module.exports = app;