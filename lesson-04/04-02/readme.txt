###########################
Application Object
###########################

var express = require('express');
var app = express();

// set environment variable
app.set(name, value)

// get environment variable
app.get(name)

// middleware for mounting request path
app.use([path], callback)


---------------------------
// routing
//GET method (search)
app.get(path, callback)

//POST method (insert)
app.post(path, callback)

//PUT method (update)
app.put(path, callback)

//DELETE method (DELETE)
app.delete(path, callback)

//Consolidate in route path separate by request method
app.route(path)
    .get(callback)
    .post(callback)

###########################
Request Object
###########################    
var helloWorld = function(req, res, next){ };

req.query => Query string
req.params => routing parameter
req.body => form body
req.path => current request path 
req.host => server name 
req.ip => client ip 

###########################
Response Object
########################### 
var helloWorld = function(req, res, next){ };

res.status(code) => http status code
res.set(field, [value]) => set http response header
res.redirect([status], url) => redirect to new url with http status code
res.send([body|status], [body]) => send response with content type auto.
res.json([body|status], [body]) => send response specific json type
res.render(view, [local], callback) => render view and send html response