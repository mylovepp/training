var connect = require('connect');
var app = connect();

//middleware
app.use(function (req, res, next) {
    //next is middleware function
    res.setHeader('Content-Type', 'text/plain');
    res.write('Hello World');
    res.end();
});


app.listen(8080);

console.log('Server running as http://localhost:8080');