var connect = require('connect');
var app = connect();

//middleware
var logger = function (req, res, next) {
    console.log(req.method, req.url);
    next();
}

var helloWorld = function (req, res, next) {
    res.setHeader('Content-Type', 'text/plain');
    res.write('Hello World');
    res.end();
}

var helloCignaWorld = function (req, res, next) {
    res.setHeader('Content-Type', 'text/plain');
    res.write('Hello Cigna World');
    res.end();
}

//mounting request path
app.use(logger);
app.use('/hello', helloWorld);
app.use('/cigna', helloCignaWorld);


app.listen(8080);

console.log('Server running as http://localhost:8080');