var connect = require('connect');
var app = connect();

//middleware
var logger = function (req, res, next) {
    console.log(req.method, req.url);
    next();
}

var helloWorld = function (req, res, next) {
    res.setHeader('Content-Type', 'text/plain');
    res.write('Hello World');
    res.end();
}

app.use(logger);
app.use(helloWorld);


app.listen(8080);

console.log('Server running as http://localhost:8080');